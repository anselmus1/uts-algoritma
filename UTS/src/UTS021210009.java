
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author anselbegal
 */
public class UTS021210009 {
      public static void main(String[] args) {
        String npm, nama, prodi;
        Scanner SC = new Scanner(System.in);
        System.out.print("Silahkan masukkan NAMA anda: ");
        nama = SC.nextLine();
        System.out.println("NAMA anda: " + nama);
        System.out.print("Silahkan masukkan NPM anda: ");
        npm = SC.nextLine();
        System.out.println("NPM anda: " + npm);
        System.out.print("Silahkan masukkan PRODI anda: ");
        prodi = SC.nextLine();
        System.out.println("PRODI anda: " + prodi);
        String grade = null;
        Scanner sc = new Scanner(System.in);
        System.out.print("Silahkan masukkan nilai: ");
        int nilai = sc.nextInt();
        System.out.println("Nilai anda adalah " + nilai);
        if (nilai >= 70) {
            grade = "Lulus";
        } else if (nilai < 70) {
            grade = "Gagal";
        }
        System.out.println("Grade anda adalah " + grade);
    }
}
